import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const portId = 4000;
  const app = await NestFactory.create(AppModule);
  await app.listen(portId);
  console.log('running at ', portId);
}
bootstrap();
